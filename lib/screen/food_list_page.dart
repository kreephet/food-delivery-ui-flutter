import 'package:flutter/material.dart';

class FoodListPage extends StatefulWidget {
  const FoodListPage({Key? key}) : super(key: key);

  @override
  State<FoodListPage> createState() => _FoodListPageState();
}

class _FoodListPageState extends State<FoodListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFCFCFC),
      appBar: AppBar(
        backgroundColor: Color(0xffFCFCFC),
        leading: Icon(
          Icons.menu,
          color: Colors.black,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              Icons.shopping_cart_outlined,
              color: Colors.black,
            ),
          )
        ],
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          children: [
            itemPager(),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: GridView.builder(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 18,
                      mainAxisExtent: 280,
                      crossAxisSpacing: 18),
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return itemFood();
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget itemPager() {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: Colors.orange),
            height: 250,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            height: 180,
            width: 250,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/burger-1.png"))),
          ),
        ),
        Positioned(
          top: 50,
          left: 50,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "50%",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 57,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              const SizedBox(
                width: 150,
                child: Text(
                  "offer is only for today, so place order right now.",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                    height: 42,
                    width: MediaQuery.of(context).size.width * 0.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        gradient: const LinearGradient(
                            colors: [
                              Color(0xff8E8E8E),
                              Color(0xff000000),
                              Color(0xff000000),
                              Color(0xff000000)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter)),
                    child: const Center(
                      child: Text(
                        "Order Now",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget itemFood() {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Image.asset("assets/images/burger_img_1.png"),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Double Layer Burger",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et, interdum leo, urna amet.",
                    style: TextStyle(fontSize: 10)),
                ElevatedButton(
                  onPressed: () {},
                  child: Text("Order Now"),
                  style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.orange),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
