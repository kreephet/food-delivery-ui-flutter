import 'package:flutter/material.dart';
import 'package:food_app/screen/food_list_page.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 72),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .5,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/intro_img.png"))),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * .5,
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(50))),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .45,
                    child: Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(50.0),
                          child: Column(
                            children: const [
                              Text(
                                "#1",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 50,
                                    color: Colors.orange,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic,
                                    decoration: TextDecoration.underline),
                              ),
                              Text("Worlds best Food Ordering app",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 33)),
                              Text(
                                  "We are awarded with best food ordering app world wide.",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: Container(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => const FoodListPage()));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(26.0),
                    child: Container(
                        height: 62,
                        width: 377,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            gradient: const LinearGradient(
                                colors: [
                                  Color(0xffFFD5B2),
                                  Color(0xffFB923C),
                                  Color(0xffFB923C),
                                  Color(0xffFB923C)
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter)),
                        child: const Center(
                          child: Text(
                            "Get Started Now",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        )),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
